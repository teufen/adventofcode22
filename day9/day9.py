sign = lambda a: (a>0) - (a<0)

def get_new_tail_coord(tail, head):
    h_distance = head[0] - tail[0]
    v_distance = head[1] - tail[1]
    if (-1 <= h_distance <= 1) and (-1 <= v_distance <= 1):
        # no move of the tail
        return tail
    elif h_distance == 0:
        tail = [tail[0], tail[1] + sign(v_distance)]
    elif v_distance == 0:
        tail = [tail[0] + sign(h_distance), tail[1]]
    else:
        tail = [tail[0] + sign(h_distance), tail[1] + sign(v_distance)]
    return tail


# coordinates [x, y]
coord_H = [0, 0]
coord_T = [0, 0]
coords = [[0, 0] for _ in range(10)]
visited_T = [[0, 0]]
#for line in open('test.txt', 'r'):
for line in open('input.txt', 'r'):
#for line in open('test2.txt', 'r'):
    [direct, steps] = line.strip('\n').split(' ')
    for s in range(1, int(steps) + 1):
        if direct == 'U':
            coords[0][1] += 1
        elif direct == 'D':
            coords[0][1] -= 1
        elif direct == 'R':
            coords[0][0] += 1
        elif direct == 'L':
            coords[0][0] -= 1
        for knot in range(1, len(coords)):
            coords[knot] = get_new_tail_coord(coords[knot], coords[knot - 1])
        #coord_T = get_new_tail_coord(coord_T, coord_H)
        #if coord_T not in visited_T:
        if coords[-1] not in visited_T:
            visited_T.append(coords[-1])
print len(visited_T)

