class Location():
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def coord(self):
        return [self.x, self.y]


class Beacon(Location):
    pass


class Sensor(Location):
    def __init__(self, x, y, beacon):
        Location.__init__(self, x, y)
        self.beacon = beacon

    def get_free_range(self):
        return distance(self, self.beacon)


def distance(a, b):
    return abs(a.x - b.x) + abs(a.y - b.y)


def flatten(lst):
    lst = sorted(lst, key=lambda a:a[0])
    i = len(lst) - 1
    while i > 0:
        left = lst[i-1]
        right = lst[i]
        if left[1] >= right[0]:
            if left[1] >= right[1]:
                lst.remove(right)
            else:
                lst.remove(right)
                lst[i-1] = [left[0], right[1]]
        i -= 1
    while i < len(lst) - 1:
        left = lst[i]
        right = lst[i + 1]
        if left[1] >= right[0]:
            if left[1] >= right[1]:
                lst.remove(right)
            else:
                lst.remove(right)
                lst[i] = [left[0], right[1]]
        else:
            i += 1
    return lst


import re
#content = open('test.txt', 'r').readlines()
#test_range = 20
content = open('input.txt', 'r').readlines()
test_range = 4000000
free_ranges_per_row = {}
loopcount = 0
sensorcount = 0

for line in content:
    sensorcount += 1
    p = re.compile(r'\D+[=](?P<sx>[-]?\d+)\D+[=](?P<sy>[-]?\d+):\D+[=](?P<bx>[-]?\d+)\D+[=](?P<by>[-]?\d+)')
    m = p.search(line.strip('\n'))
    beacon = Beacon(int(m.group('bx')), int(m.group('by')))
    sensor = Sensor(int(m.group('sx')), int(m.group('sy')), beacon)
    # check if sensor covers (part of) test_range grid
    ran = sensor.get_free_range()
    if (sensor.x + ran < 0 or 
            sensor.x - ran > test_range or
            sensor.y + ran < 0 or
            sensor.y - ran > test_range):
        print 'sensor {:d} does not cover test_range, skipping...'.format(sensorcount)
        continue
    print 'sensor {:d} with y-range {:d}.'.format(
            sensorcount, ran)
    if sensor.y < ran:
        yrange = range(sensor.y, sensor.y + ran + 1)
        d = -1
    elif sensor.y >= ran:
        yrange = range(sensor.y - ran + 1, sensor.y)
        d = 1
    for j, y in enumerate(yrange):
        range_min = max(0, sensor.x - (ran - abs(sensor.y - y)))
        range_max = min(test_range, sensor.x + (ran - abs(sensor.y - y)))
        if range_min <= range_max:
            #if [range_min, range_max] == [0, test_range]:
                #continue
            for _y in [y, y + j * d]:
                if _y not in range(test_range + 1):
                    continue
                if _y not in free_ranges_per_row:
                    free_ranges_per_row[_y] = [[range_min, range_max]]
                else:
                    free_ranges_per_row[_y].append([range_min, range_max])
                #free_ranges_per_row[_y] = flatten(free_ranges_per_row[_y])

for y in sorted(free_ranges_per_row.keys()):
    free_ranges_per_row[y] = flatten(free_ranges_per_row[y])
    if free_ranges_per_row[y] == [[0, test_range]]:
        continue
    else:
        if len(free_ranges_per_row[y]) == 1:
            if free_ranges_per_row[y][0][0] == 0:
                x = free_ranges_per_row[y][0][-1]
            else:
                x = 0
        else:
            x = free_ranges_per_row[y][0][1] + 1
        break

print [x, y]
print (x * 4000000 + y)
