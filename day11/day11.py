#content = open('test.txt', 'r').readlines()
content = open('input.txt', 'r').readlines()
monkeys = []
monkey_links = {}

class Monkey():
    def __init__(self, i, start_items, operation, test):
        self.id = i
        self.items = start_items
        self.operation = operation
        self.test = test
        self.inspected = 0

    def add_targets(self, targets):
        self.true = monkeys[targets[0]]
        self.false = monkeys[targets[1]]


def parse_operation(op):
    '{old * i, old + i, old * old}'
    parts = op.split(' ')
    if parts[2] == 'old':
        ops = lambda a: a * a
    elif parts[1] == '+':
        ops = lambda a: a + int(parts[2])
    elif parts[1] == '*':
        ops = lambda a: a * int(parts[2])
    return ops


def parse_bloc(bloc):
    '''
    Monkey 0:
      Starting items: 79, 98
      Operation: new = old * 19
      Test: divisible by 23
        If true: throw to monkey 2
        If false: throw to monkey 3
    '''
    _index = int(bloc[0].split(' ')[1].strip(':\n'))
    items = [int(x) for x in bloc[1].lstrip('  Starting items: ').strip('\n').split(', ')]
    operation = parse_operation(bloc[2].lstrip('  Operation: new = ').strip('\n'))
    test = int(bloc[3].lstrip('  Test: divisible by ').strip('\n'))
    m = Monkey(_index, items, operation, test)
    monkeys.append(m)
    targets = []
    for i in range(2):
        targets.append(int(bloc[4 + i].lstrip(' ').strip('\n').split(' ')[5]))
    monkey_links[_index] = targets
    return


for bloc in range(len(content) / 7 + 1):
    parse_bloc(content[bloc * 7:(bloc + 1) * 7 - 1])
div_product = 1
for index, monkey in enumerate(monkeys):
    monkey.add_targets(monkey_links[index])
    div_product *= monkey.test

for round in range(10000):
    for monkey in monkeys:
        for item in monkey.items:
            monkey.items = monkey.items[1:]
            item = monkey.operation(item)%div_product
            #item /= 3
            if item%monkey.test == 0:
                monkey.true.items.append(item)
            else:
                monkey.false.items.append(item)
            monkey.inspected += 1

m = sorted(monkeys, key=lambda x: x.inspected, reverse=True)
print m[0].inspected * m[1].inspected
