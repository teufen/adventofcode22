for line in open('input.txt', 'r'):
    inputstr = line.strip('\n')


def has_repeats(b):
    for i in range(len(b)):
        if b[i] in b[i+1:]:
            return True
    else:
        return False


def get_buffer_index(string):
    for i, c in enumerate(string):
        b = string[i:i+14]
        if not has_repeats(b):
            return i+14
    # if we are here, there is no pattern in the string
    return None

print get_buffer_index(inputstr)

