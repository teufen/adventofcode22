listtype = type([])
inttype = type(0)

def in_right_order(l, r):
    if type(l) == listtype and type(r) == listtype:
        if len(l) == 0 and len(l) < len(r):
            return True
        for i in range(len(l)):
            if type(l[i]) == inttype:
                try:
                    if type(r[i]) == inttype:
                        if l[i] < r[i]:
                            return True
                        elif l[i] > r[i]:
                            return False
                        else:
                            continue
                    elif type(r[i]) == listtype:
                        tmp = in_right_order([l[i]], r[i])
                except IndexError:
                    return False
            else:
                try:
                    if type(r[i]) == listtype:
                        tmp = in_right_order(l[i], r[i])
                    elif type(r[i]) == inttype:
                        tmp = in_right_order(l[i], [r[i]])
                except IndexError:
                    return False
            if tmp == None:
                continue
            else:
                return tmp
        if len(l) < len(r):
            return True
        else:
            # l and r are completely equal
            return None


#content = open('test.txt', 'r').readlines()
content = open('input.txt', 'r').readlines()

# part one
index = 1
right_orders = []
left = []
right = []
for i, line in enumerate(content):
    line = line.strip('\n')
    if line == '':
        if in_right_order(left, right):
            right_orders.append(index)
        index += 1
    elif i%3 == 0:
        left = eval(line)
    else:
        right = eval(line)
if in_right_order(left, right):
    right_orders.append(index)
print sum(right_orders)

def add_in_place(lst, packet):
    for i in range(len(packets)):
        if in_right_order(packets[i], packet):
            continue
        else:
            return lst[:i] + [packet] + lst[i:]
    return lst + [packet]

# part two
packets = [[[2]], [[6]]]
for line in content:
    line = line.strip('\n')
    if line == '':
        continue
    else:
        packet = eval(line)
        packets = add_in_place(packets, packet)
print str((packets.index([[2]]) + 1) * (packets.index([[6]]) + 1))
