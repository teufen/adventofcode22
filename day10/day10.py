# day one
cycle = 0
x = 1
checkpoints = [20 + i * 40 for i in range(6)]
rssi = 0
#for line in open('test.txt', 'r'):
for line in open('input.txt', 'r'):
    if line[:4] == 'noop':
        cycle += 1
        if cycle in checkpoints:
            rssi += cycle * x
    elif line[:4] == 'addx':
        [cmd, value] = line.strip('\n').split(' ')
        for _ in range(2):
            cycle += 1
            if cycle in checkpoints:
                rssi += cycle * x
        x += int(value)
print rssi


# day two
crt = [['.' for _ in range(40)] for _ in range(6)]
sprite = [0, 1, 2]
row = 0
cycle = -1
#for line in open('test.txt', 'r'):
for line in open('input.txt', 'r'):
    if line[:4] == 'noop':
        cycle += 1
        if cycle >= len(crt[0]):
            cycle = 0
            row += 1
        if cycle in sprite:
            crt[row][cycle] = '#'
    elif line[:4] == 'addx':
        [cmd, value] = line.strip('\n').split(' ')
        for _ in range(2):
            cycle += 1
            if cycle >= len(crt[0]):
                cycle = 0
                row += 1
            if cycle in sprite:
                crt[row][cycle] = '#'
        sprite = [i + int(value) for i in sprite]
for row in crt:
    s = ''
    for p in row:
        s += p 
    print s
