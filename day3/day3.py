#priorities are the indexes
aprios = ['dummy'] + [chr(n) for n in range(ord('a'), ord('z') + 1)] +\
	   [chr(n) for n in range(ord('A'), ord('Z') + 1)]
prio_sum = 0
#for line in open('test.txt', 'r'):
for line in open('input.txt', 'r'):
    line = line.strip()
    stringlength = len(line) / 2
    a = line[:stringlength]
    b = line[-stringlength:]
    common = ''
    for e in set(a).intersection(b):
        common += e
    prio_sum += aprios.index(common)
print prio_sum
team_index = 0
team = [''] * 4
prio_sum = 0
#for line in open('test2.txt', 'r'):
for line in open('input.txt', 'r'):
    team[team_index] = line.strip()
    team_index += 1
    if team_index == 3:
        common = ''
        for e in set(set(team[0]).intersection(team[1])).intersection(team[2]):
            common += e
        prio_sum += aprios.index(common)
        team_index = 0
print prio_sum
