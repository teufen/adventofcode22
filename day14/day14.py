rock = '#'
air = '.'
sand = 'o'
insert = 500

class Grid():
    def __init__(self, x_min, x_max, y_min, y_max):
        self.x_min = x_min
        self.x_max = x_max
        self.y_min = y_min
        self.y_max = y_max
        self._grid = []
        for _ in range(x_max - x_min + 1):
            l = []
            for _ in range(y_max - y_min + 1):
                l.append('.')
            self._grid.append(l)

    def get(self, coord):
        if coord[0] < self.x_min or coord[1] < self.y_min:
            raise IndexError(str(coord) + ' is not on the grid.')
        return self._grid[coord[0] - self.x_min][coord[1]]

    def get_left(self, coord):
        return self.get([coord[0] - 1, coord[1]])

    def get_right(self, coord):
        return self.get([coord[0] + 1, coord[1]])

    def put(self, coord, substance):
        if substance == sand and self.get(coord) <> air:
            self.print_grid()
            raise Exception(str(coord) + ' is not empty.')
        self._grid[coord[0] - self.x_min][coord[1]] = substance

    def print_grid(self):
        row_preamble = len(str(self.y_max)) + 1
        for i in range(len(str(self.x_max))):
            s = ' ' * row_preamble
            s += str(self.x_min)[i]
            s += ' ' * (insert - self.x_min - 1)
            s += str(insert)[i]
            s += ' ' * (self.x_max - insert - 1)
            s += str(self.x_max)[i]
            print s
        f = '{:>' + str(len(str(self.y_max))) + 'd} '
        for y in range(self.y_max + 1):
            s = f.format(y)
            for x in range(self.x_max - self.x_min + 1):
                s += self._grid[x][y]
            print s


#content = [line.strip('\n').split(' -> ') for line in open('test.txt', 'r').readlines()]
content = [line.strip('\n').split(' -> ') for line in open('input.txt', 'r').readlines()]
# determin grid dimensions
xx = []
yy = []
for row in content:
    for el in row:
        [x, y] = [int(p) for p in el.split(',')]
        xx.append(x)
        yy.append(y)
xx = sorted(xx)
yy = sorted(yy)
ymax = yy[-1] + 2
xmin = min(xx[0], insert - ymax)
xmax = max(xx[-1], insert + ymax)
grid = Grid(xmin, xmax, 0, ymax)
# place rocks in grid
for rocks in content:
    for pair in [rocks[i:i + 2] for i in range(len(rocks) - 1)]:
        start = [int(p) for p in pair[0].split(',')]
        end = [int(p) for p in pair[1].split(',')]
        [start, end] = sorted([start, end])
        if start[0] == end[0]:
            # horizontal rock
            for i in range(start[1],end[1] + 1):
                grid.put([start[0], i], rock)
        elif start[1] == end[1]:
            # vertical rock
            for i in range(start[0],end[0] + 1):
                grid.put([i, start[1]], rock)
# place rock floor
for x in range(xmax-xmin + 1):
    grid.put([xmin + x, ymax], rock)
# start dropping sand
sand_units = 0
cycles = -1
while cycles < sand_units and grid.get([insert, grid.y_min]) <> sand:
    cycles += 1
    loc = [insert, grid.y_min]
    while loc[1] <= grid.y_max:
        if grid.get(loc)  == air:
            loc[1] += 1
        else:
            if grid.get_left(loc) == air:
                loc[0] -= 1
            elif grid.get_right(loc) == air:
                loc[0] += 1
            else:
                loc[1] -= 1
                grid.put(loc, sand)
                sand_units += 1
                loc[1] = grid.y_max + 1
grid.print_grid()
print sand_units
