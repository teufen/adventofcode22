count = 0
#for line in open('test.txt', 'r'):
for line in open('input.txt', 'r'):
    [[a1, a2], [b1, b2]] = sorted([map(int, l) for l in [i.split('-') for i in line.strip().split(',')]])
    if a2 >= b2:
        count += 1
    elif a1 == b1:
        count += 1
    elif a2 >= b1:
        # for part 2
        count += 1
print count
