def treescore_dir(tree, c, d):
    score = 1
    if d == 'up':
        if c[0] == 0:
            score -= 1
            return score
        newc = [c[0] - 1, c[1]]
        neighbour = tree_map[c[0] - 1][c[1]]
    elif d == 'down':
        if c[0] == len(tree_map) - 1:
            score -= 1
            return score
        newc = [c[0] + 1, c[1]]
        neighbour = tree_map[c[0] + 1][c[1]]
    elif d == 'left':
        if c[1] == 0:
            score -= 1
            return score
        newc = [c[0], c[1] - 1]
        neighbour = tree_map[c[0]][c[1] - 1]
    elif d == 'right':
        if c[1] == len(tree_map[0]) - 1:
            score -= 1
            return score
        newc = [c[0], c[1] + 1]
        neighbour = tree_map[c[0]][c[1] + 1]
    if neighbour < tree:
        score += treescore_dir(tree, newc, d)
    return score


def treescore(tree, coord):
    score = treescore_dir(tree, coord, 'up')
    score *= treescore_dir(tree, coord, 'down')
    score *= treescore_dir(tree, coord, 'left')
    score *= treescore_dir(tree, coord, 'right')
    return score


def visible_dir(tree, c, d):
    if d == 'up':
        if c[0] == 0:
            return True
        newc = [c[0] - 1, c[1]]
        neighbour = tree_map[c[0] - 1][c[1]]
    elif d == 'down':
        if c[0] == len(tree_map) - 1:
            return True
        newc = [c[0] + 1, c[1]]
        neighbour = tree_map[c[0] + 1][c[1]]
    elif d == 'left':
        if c[1] == 0:
            return True
        newc = [c[0], c[1] - 1]
        neighbour = tree_map[c[0]][c[1] - 1]
    elif d == 'right':
        if c[1] == len(tree_map[0]) - 1:
            return True
        newc = [c[0], c[1] + 1]
        neighbour = tree_map[c[0]][c[1] + 1]
    if neighbour < tree:
        return visible_dir(tree, newc, d)
    else:
        return False


def visible(tree, coord):
    if visible_dir(tree, coord, 'up'):
        return True
    elif visible_dir(tree, coord, 'down'):
        return True
    elif visible_dir(tree, coord, 'left'):
        return True
    elif visible_dir(tree, coord, 'right'):
        return True
    else:
        return False


tree_map = []
#for line in open('test.txt', 'r'):
for line in open('input.txt', 'r'):
    row = []
    for e in line.strip('\n'):
        row.append(int(e))
    tree_map.append(row)
# part one
vis_trees = 2 * len(tree_map) + 2 * (len(tree_map[0]) - 2)
for r in range(1, len(tree_map) - 1):
    for t in range(1, len(tree_map[0]) -1):
        if visible(tree_map[r][t], [r, t]):
            vis_trees += 1
print vis_trees
# part two
highscore = 0
for r in range(1, len(tree_map) - 1):
    for t in range(1, len(tree_map[0]) -1):
        score = treescore(tree_map[r][t], [r, t])
        if score > highscore:
            highscore = score
print highscore
