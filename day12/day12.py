from Queue import PriorityQueue

letters = ['S'] + [chr(n) for n in range(ord('a'), ord('z') + 1)] + ['E']

class Node():
    def __init__(self, x, y, value):
        self.x = x
        self.y = y
        self.value = value


def heuristic(a, b):
   # Manhattan distance on a square grid
   return abs(a.x - b.x) + abs(a.y - b.y)


def neighbours(n):
    ns = []
    if n.y < len(el[0]) - 1:
        node = nodes[n.x][n.y + 1]
        if node.value in letters[:letters.index(n.value) + 2]:
            ns.append(node)
    if n.x < len(el) - 1:
        node = nodes[n.x + 1][n.y]
        if node.value in letters[:letters.index(n.value) + 2]:
            ns.append(node)
    if n.y > 0:
        node = nodes[n.x][n.y - 1]
        if node.value in letters[:letters.index(n.value) + 2]:
            ns.append(node)
    if n.x > 0:
        node = nodes[n.x - 1][n.y]
        if node.value in letters[:letters.index(n.value) + 2]:
            ns.append(node)
    return ns


def get_shortest_path_length(begin, end):
    frontier = PriorityQueue()
    frontier.put(begin, 0)
    came_from = {}
    came_from[begin] = None
    cost_so_far = {}
    cost_so_far[begin] = 0
    while not frontier.empty():
        current = frontier.get()
        for next in neighbours(current):
            new_cost = cost_so_far[current] + 1
            if next not in cost_so_far or new_cost < cost_so_far[next]:
                cost_so_far[next] = new_cost
                priority = new_cost + heuristic(end, next)
                frontier.put(next, priority)
                came_from[next] = current
    return cost_so_far[end]


el = open('input.txt', 'r').readlines()
# fill data and locate S and E
nodes = []
for i, line in enumerate(el):
    nodes.append([])
    for j, location in enumerate(line):
        if location == 'S':
            start = Node(i, j, el[i][j])
            nodes[i].append(start)
        elif location == 'E':
            target = Node(i, j, el[i][j])
            nodes[i].append(target)
        else:
            nodes[i].append(Node(i, j, el[i][j]))


# part one
print get_shortest_path_length(start, target)

# part two
a_nodes = []
for row in nodes:
    for node in row:
        if node.value == 'a':
            neighs = neighbours(node)
            for n in neighs:
                if n.value in ['a', 'S']:
                    neighs.remove(n)
            if len(neighs) == 0:
                continue
            elif 'b' not in [n.value for n in neighs]:
                continue
            else:
                a_nodes.append(node)
min_path = 1000
for node in a_nodes:
    try:
        path = get_shortest_path_length(node, target)
        if path < min_path:
            min_path = path
    except KeyError:
        continue
print min_path
