def get_pos_in_list(pos, a):
    return a[(a.index(0) + pos)%len(a)]

def move_one_step_forward(el, a):
    loc = a.index(el)
    if loc == len(a) - 1:
        a = [a[0]] + [el] + a[1:-1]
    elif loc == len(a) - 2:
        a = [el] + a[:loc] + [a[-1]]
    else:
        a = a[:loc] + [a[loc + 1]] + [a[loc]] + a[loc +2:]
    return a

def move_one_step_backward(el, a):
    loc = a.index(el)
    if loc == 0:
        a = a[1:-1] + [el] + [a[-1]]
    elif loc == 1:
        a = [a[0]] + a[2:] + [a[loc]]
    else:
        a = a[:loc - 1] + [a[loc]] + [a[loc - 1]] + a[loc + 1:]
    return a

#orig = [int(line.strip('\n')) for line in open('test.txt', 'r').readlines()]
orig = [int(line.strip('\n')) for line in open('input.txt', 'r').readlines()]
new = [i for i in orig]

#print new
for el in orig:
    #print el
    if el == 0:
        continue
    elif el < 0:
        for _ in range(abs(el)%len(orig)):
            new = move_one_step_backward(el, new)
    elif el > 0:
        for _ in range(el%len(orig)):
            new = move_one_step_forward(el, new)
    #print new

coordsum = get_pos_in_list(1000, new)
coordsum += get_pos_in_list(2000, new)
coordsum += get_pos_in_list(3000, new)
print coordsum
