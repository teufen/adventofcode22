limit = 100000
dirs = {}
pwd = ''
curdepth = 0
#for line in open('test.txt', 'r'):
for line in open('input.txt', 'r'):
    line = line.strip('\n')
    if line == '$ cd ..':
        parts = pwd.split('/')
        pwd = pwd[:(-len(parts[-2]) -1)]
        curdepth -= 1
    elif line[0:4] == '$ cd':
        # we enter a new directory
        if line[5] == '/':
            pwd = line[5:]
        else:
            pwd += line[5:] + '/'
        curdepth += 1
        if pwd not in dirs.keys():
            dirs[pwd] = {'depth': curdepth, 'size': 0, 'childeren': [],}
    elif line[0:4] == '$ ls':
        continue
    elif line[0:3] == 'dir':
        # this is a child of the current directory
        dirs[pwd]['childeren'].append(pwd + line[4:] + '/')
    else:
        dirs[pwd]['size'] += int(line.split(' ')[0])

for key, value in sorted(dirs.items(), key=lambda x:x[1]['depth'], reverse=True):
    if len(dirs[key]['childeren']) > 0:
            for child in dirs[key]['childeren']:
                dirs[key]['size'] += dirs[child]['size']

# part one
total = 0
for d in dirs.keys():
    if dirs[d]['size'] <= limit:
        total += dirs[d]['size']
print total

# part two
disc = 70000000
needed = 30000000
available = disc - dirs['/']['size']
to_delete = needed - available
for key, value in sorted(dirs.items(), key=lambda x:x[1]['size']):
    if dirs[key]['size'] >= to_delete:
        print dirs[key]['size']
        break
