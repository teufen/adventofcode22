elf = 1
d = {elf: 0}
for line in open('input.txt', 'r'):
    l = line.strip()
    if l == '':
        # we have finished an elf
        elf += 1
        d[elf] = 0
    else:
        d[elf] += int(l)
se = sorted(d.items(), key=lambda x:x[1], reverse=True)
print 'top elf has ' + str(se[0][1])
s = 0
for i in (ses[1] for ses in se[:3]):
    s += i
print 'Top 3 elves have ' + str(s)
