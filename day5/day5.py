import re

reading_moves = False
stacks = []
#for line in open('test1.txt', 'r'):
for line in open('input.txt', 'r'):
    # read one line to get number of stacks
    nb_of_stacks = (len(line.strip('\n')) + 1) / 4
    break
for n in range(nb_of_stacks):
    stacks.append([])
#for line in open('test1.txt', 'r'):
for line in open('input.txt', 'r'):
    line = line.strip('\n')
    if line == '':
        # we have reached the end of the stacks, next we read moves
        reading_moves = True
        for n in range(nb_of_stacks):
            stacks[n].pop()
            stacks[n].reverse()
        continue
    if not reading_moves:
        for n in range(nb_of_stacks):
            crate = line[(n * 4):((n + 1) * 4 )].strip().strip('[').strip(']')
            if crate <> '':
                stacks[n].append(crate)
    else:
        # move 2 from 3 to 2
        p = re.compile(r'\D+(?P<nb>\d+)\D+(?P<frm>\d+)\D+(?P<to>\d+)\D*')
        m = p.search(line)
        tmpstack = []
        for i in range(int(m.group('nb'))):
            #stacks[int(m.group('to')) - 1].append(stacks[int(m.group('frm')) - 1].pop())
            tmpstack.append(stacks[int(m.group('frm')) - 1].pop())
        for i in range(int(m.group('nb'))):
            stacks[int(m.group('to')) - 1].append(tmpstack.pop())

s = ''
for n in range(nb_of_stacks):
    s += stacks[n].pop() 
print s
