def get_line_score(opp, me):
    score = 0
    if me == 'X':
        # loose 
        if opp == 'A':
            score += 3
        elif opp == 'B':
            score += 1
        elif opp == 'C':
            score += 2
    elif me == 'Y':
        # draw
        score += 3
        if opp == 'A':
            score += 1
        elif opp == 'B':
            score += 2
        elif opp == 'C':
            score += 3
    elif me == 'Z':
        # win
        score += 6
        if opp == 'A':
            score += 2
        elif opp == 'B':
            score += 3
        elif opp == 'C':
            score += 1
    return score

score = 0
for line in open('input.txt', 'r'):
    l = line.strip()
    [opp, me] = l.split(' ')
    score += get_line_score(opp, me)
print score
