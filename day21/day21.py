def calculate_new_target(target, int_ops, value):
    if int_ops == '+':
        new_target =  target - int(value)
    elif int_ops == '*':
        new_target = target / int(value)
    return new_target

class Tree():
    def __init__(self):
        self.left = None
        self.right = None
        self.ops = None
        self.value = None

    def calculate_value(self):
        if self.left.value == None:
            self.left.calculate_value()
        if self.right.value == None:
            self.right.calculate_value()
        self.value = str(eval(self.left.value + self.ops + self.right.value))

    def find_missing(self, target):
        if self.value == 'humn':
            # this is the place
            self.value = target
        elif self.left.value == None or self.left.value == 'humn':
            # target =  new_target self.ops self.right.value
            if self.ops == '/':
                new_target = target * int(self.right.value)
            elif self.ops == '-':
                new_target = target + int(self.right.value)
            else: 
                new_target = calculate_new_target(target, self.ops, int(self.right.value))
            self.left.find_missing(new_target)
        elif self.right.value == None or self.right.value == 'humn':
            # target = self.left.value self.ops new_target
            if self.ops == '/':
                new_target = int(self.left.value) / target
            elif self.ops == '-':
                new_target = int(self.left.value) - target
            else: 
                new_target = calculate_new_target(target, self.ops, int(self.left.value))
            self.right.find_missing(new_target)
        else:
            print 'we are at a dead end:'
            print 'left = ' + self.left.value
            print 'right = ' + self.right.value


monkeys = {}
#for line in open('test.txt', 'r'):
for line in open('input.txt', 'r'):
    a = line.strip('\n').split(' ')
    name = a[0].rstrip(':')
    if name not in monkeys:
        monkeys[name] = Tree()
    if len(a) == 4:
        # monkey with two dependencies
        if a[1] not in monkeys:
            monkeys[a[1]] = Tree()
        monkeys[name].left = monkeys[a[1]]
        monkeys[name].ops = a[2]
        if a[3] not in monkeys:
            monkeys[a[3]] = Tree()
        monkeys[name].right = monkeys[a[3]]
    elif len(a) == 2:
        # monkey with number
        monkeys[name].value = a[1]
    if name == 'root':
        monkeys[name].ops = '=='
    if name == 'humn':
        monkeys[name].value = name

#resolve values where possible
for name,monkey in monkeys.items():
    if monkey.value <> None:
        continue
    else:
        try:
            monkey.calculate_value()
        except:
            continue


root = monkeys['root']
if root.left.value == None:
    root.left.find_missing(int(root.right.value))
elif root.right.value == None:
    root.right.find_missing(int(root.left.value))

print 'humn should yell ' + str(monkeys['humn'].value)
